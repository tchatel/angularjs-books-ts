import * as angular from "angular";
import "/node_modules/angular-i18n/angular-locale_fr-fr.js";
import {BooksController} from "./books.controller";


export let appModule = angular.module('app', [])
  .controller('BooksController', BooksController);
