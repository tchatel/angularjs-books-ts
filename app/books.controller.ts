import {OrderRow} from "./order-row.model";

export class BooksController {

  rows: OrderRow[] = [
    new OrderRow("La Horde du Contrevent", "Alain Damasio", 10.90, 3),
    new OrderRow("Black-out", "Connie Willis", 9.90, 2)
  ];

  constructor() {
  }

}



