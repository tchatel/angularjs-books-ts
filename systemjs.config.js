/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function(global) {

    // map tells the System loader where to look for things
    var map = {
        'app':                      'app',
        'angular':                  'node_modules/angular',
        'angular-animate':          'node_modules/angular-animate/',
        'angular-loader':           'node_modules/angular-loader/',
        'angular-messages':         'node_modules/angular-messages/',
        'angular-mocks':            'node_modules/angular-mocks/',
        'angular-route':            'node_modules/angular-route/',
        'angular-sanitize':         'node_modules/angular-sanitize/',
        'angular-ui-router':        'node_modules/angular-ui-router/',
        'rxjs':                     'node_modules/rxjs'
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'app':                      { main: 'main.js', defaultExtension: 'js' },
        'angular':                  { main: 'index.js', defaultExtension: 'js' },
        'angular-animate':          { main: 'index.js', defaultExtension: 'js' },
        'angular-loader':           { main: 'index.js', defaultExtension: 'js' },
        'angular-messages':         { main: 'index.js', defaultExtension: 'js' },
        'angular-mocks':            { main: 'index.js', defaultExtension: 'js' },
        'angular-route':            { main: 'index.js', defaultExtension: 'js' },
        'angular-sanitize':         { main: 'index.js', defaultExtension: 'js' },
        'angular-ui-router':        { main: 'index.js', defaultExtension: 'js' },
        'rxjs':                     { defaultExtension: 'js' }
    };

    var config = {
        map: map,
        packages: packages
    };
    System.config(config);

})(this);
